Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: Hydrogen
Upstream-Contact: https://github.com/vector-im/hydrogen-web/issues
Source: https://github.com/vector-im/hydrogen-web
 .
 Repackaged, excluding source-less fonts.
Files-Excluded:
 src/platform/web/ui/css/themes/element/inter/*

Files: *
Copyright:
  2020, Bruno Windels <bruno@windels.cloud>
  2020, The Matrix.org Foundation C.I.C.
License-Grant:
 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file
 except in compliance with the License.
 You may obtain a copy of the License
 at <http://www.apache.org/licenses/LICENSE-2.0>
License: Apache-2.0

Files:
 src/utils/crypto/hkdf.js
 src/utils/crypto/pbkdf2.js
Copyright:
  2018, Jun Kurihara
  2020, The Matrix.org Foundation C.I.C.
License-Grant:
 MIT LICENSE,
 See
 <https://github.com/junkurihara/jscu/blob/develop/packages/js-crypto-pbkdf/LICENSE>
License: Expat
Comment:
 Referenced URL contains the Expat license.

Files: src/utils/sortedIndex.js
Copyright:
  2020, Bruno Windels <bruno@windels.cloud>
  DocumentCloud
  Investigative Reporters & Editors
  Jeremy Ashkenas
  JS Foundation and other contributors <https://js.foundation/>
License-Grant:
 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file
 except in compliance with the License.
 You may obtain a copy of the License
 at <http://www.apache.org/licenses/LICENSE-2.0>
License-Grant:
 Released under MIT license <https://lodash.com/license>
License: Apache-2.0 and Expat
Comment:
 Referenced URL contains the Expat license.

Files: debian/*
Copyright:
  2020, Jonas Smedegaard <dr@jones.dk>
  2020, Purism SPC
License-Grant:
 This program is free software;
 you can redistribute it and/or modify it
 under the terms of the GNU General Public License
 as published by the Free Software Foundation;
 either version 3, or (at your option) any later version.
License: GPL-3+

License: Apache-2.0
License-Reference: /usr/share/common-licenses/Apache-2.0

License: Expat
 Permission is hereby granted, free of charge,
 to any person obtaining a copy
 of this software and associated documentation files
 (the "Software"),
 to deal in the Software without restriction,
 including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense,
 and/or sell copies of the Software,
 and to permit persons to whom the Software is furnished to do so,
 subject to the following conditions:
 .
 The above copyright notice and this permission notice
 shall be included
 in all copies or substantial portions of the Software.
 .
 THE SOFTWARE IS PROVIDED "AS IS",
 WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
 INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
 FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
 ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
 OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

License: GPL-3+
License-Reference: /usr/share/common-licenses/GPL-3
